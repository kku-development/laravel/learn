@extends('step2/app')

@section('title', 'Services Page')

@section('content')

<h1>Welcome to Laravel Services</h1>

<form action="/step2/service" method="POST">
    <input type="text" name="name" id="" autocomplete="off"/>
    @csrf
    <button>Add Service</button>
    <p style="color: red;">@error('name'){{ $message }}@enderror</p>
</form>

<ul>
    @forelse ($services as $service)
        <li>{{ $service->name }}</li>

    @empty
        <li>No Services</li>

    @endforelse

</ul>
@endsection
