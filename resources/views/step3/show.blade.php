@extends('step3/main')

@section('content')
<h1>Customer Details</h1>
<p><a href="/customers"><button>< Back</button></a> <a href="/customers/{{ $customer->id }}/edit"><button>> Edit</button></a></p>
<p><span>Name: </span> <strong>{{ $customer->name }}</strong></p>
<p><span>Id: </span> <strong>{{ $customer->id }}</strong></p>
<p><span>Email: </span> <strong>{{ $customer->email }}</strong></p>

<form action="/customers/{{ $customer->id }}" method="post">
    @method('delete')
    <button>Delete</button>
    @csrf
</form>
@endsection
