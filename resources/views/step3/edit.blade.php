<h1>Edit Customer</h1>

<form action="/customers/{{ $customer->id }}" method="POST">

    @method('PATCH')

    @include('step3.from')

    <button type="submit">Add New Customer</button>
</form>
