<div>
    <label for="name">name</label>
    <input type="text" name="name" autocomplete="off" value="{{ old('name') ?? $customer->name }}" />
    @error('name') <p style="color: red">{{ $message }}</p> @enderror
</div>

<div>
    <label for="email">email</label>
    <input type="text" name="email" autocomplete="off" value="{{ old('email') ?? $customer->email }}"/>
    @error('email') <p style="color: red">{{ $message }}</p> @enderror
</div>

@csrf

<table class="table table-striped table-inverse table-responsive">
    <thead class="thead-inverse">
        <tr>
            <th class="text-danger">test</th>
            <th>ts</th>
            <th>sdf</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td scope="row"></td>
                <td>sdflkj</td>
                <td>lkj</td>
            </tr>
            <tr>
                <td scope="row"></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
</table>
