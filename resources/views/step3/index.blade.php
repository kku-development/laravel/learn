@extends('step3/main')

@section('content')

<h1 class="text-danger">Customers</h1>

<p><a href="customers/create">Add New Customer</a></p>

@forelse ($customers as $customer)
    <p><a href="/customers/{{ $customer->id }}"><strong>{{ $customer->name }}</strong></a>({{ $customer->email }})</p>
@empty
    <p>No Customer to show</p>
@endforelse

@endsection
