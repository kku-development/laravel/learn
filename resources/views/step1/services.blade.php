@extends('step1/app')

@section('title', 'Services Page')

@section('content')

<h1>Welcome to Laravel Services</h1>
<ul>
    @forelse ($services as $service)
        <li>{{ $service->name }}</li>

    @empty
        <li>No Services</li>

    @endforelse

</ul>
@endsection
