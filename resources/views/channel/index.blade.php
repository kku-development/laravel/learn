<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Channel</title>
</head>
<body>
    <ul>

        @forelse ($channels as $channel)
        <li>{{ $channel->name }}</li>
        @empty
    </ul>

    @endforelse
</body>
</html>
