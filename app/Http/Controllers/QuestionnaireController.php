<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QuestionnaireController extends Controller
{
    //
    public function index()
    {
        return view('step4.index');
    }
    public function create()
    {
        return view('step4.create');
    }
}
