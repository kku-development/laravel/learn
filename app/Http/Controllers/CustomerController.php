<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CustomerController extends Controller
{
    public $test = "";
    //
    public function index()
    {
        $customers = Customer::where('active', 1)->get();
        return view('step3.index', compact('customers'));
    }

    public function create()
    {
        $customer = new Customer();
        return view('step3.create', compact('customer'));
    }

    // 고객 정보 저장
    public function store()
    {
        $customer = Customer::create($this->validateData());

        return redirect('/customers/'.$customer->id);
    }

    // 고객 상세 페이지
    public function show(Customer $customerId)
    {
        // $customer = Customer::findOrFail($customerId);
        $customer = $customerId;

        return view('step3.show', compact('customer'));
    }

    // 고객 정보 수정연결
    public function edit(Customer $customerId)
    {
        $customer = $customerId;

        return view('step3.edit', compact('customer'));
    }

    // 고객 정보 수정
    public function update(Customer $customerId)
    {

        $customerId->update($this->validateData());

        return redirect('/customers');
    }

    // 고객 정보 삭제
    public function destroy(Customer $customerId)
    {
        $customerId->delete();

        return redirect('/customers');
    }

    // 유효성 검사
    protected function validateData()
    {
        return request()->validate([
            'name' => 'required|max:15',
            'email' => 'required|email',
        ]);

    }

    // 컬럼 추가기능
    public function addColum($colum)
    {
        $this->test = $colum;
        if (Schema::hasColumn('customers',$colum)) {
            return redirect('/customers');
        } else {
            Schema::table('customers', function (Blueprint $table) {
                $table->text($this->test)->nullable();
            });
        }
        return redirect('/customers');
    }

}
