<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServiceController extends Controller
{
    //Service Index
    public function index () {

        $services = \App\Service::all();

        return view('step2.service.index', compact('services'));
    }

    public function store () {

        // 유효성 검사
        $data = request()->validate([
            'name' => 'required|min:5|max:20',
        ]);

        \App\Service::create($data);  // $data inline 가능
        // $service = new \App\Service();
        // $service->name = request('name');
        // $service->save();

        return redirect()->back();
    }
}
