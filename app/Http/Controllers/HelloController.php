<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelloController extends Controller
{
    // 헬로우 데이터 테스트
    public function index () {
        $body = "KKu Larvel Learn";
        $title = "KKu Island Controller";

        // 변수명과 key 값이 같게 사용할 경우
        return view('step1/app', compact('body', 'title'));
    }

    // 블레이드 템플릿 테스
    public function about () {
        return view('step1/about');
    }

    public function services () {

        $services = \App\Service::all();

        // $services = [
        //     'service 1',
        //     'service 2',
        //     'service 3',
        //     'service 4',
        // ];

        return view('step1/services', compact('services'));
    }
}
