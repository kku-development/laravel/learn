<?php

namespace App\Http\Controllers;

use App\Billing\PaymentGatewayContract;
use App\Orders\OrderDetails;
use Illuminate\Http\Request;

class PayOrderController extends Controller
{
    public function store(OrderDetails $orderDetails, PaymentGatewayContract $PaymentGatewayContract)
    {
        // 인자값으로 선언 가능하다
        // $paymentGateway = new BankPaymentGateway('KRW');

        $order = $orderDetails->all();
        $name = $order['name'];
        $address = $order['address'];
        // reflection class
        dd($PaymentGatewayContract->charge(2500,$name, $address));
    }
}
