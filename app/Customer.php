<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    // optione 1
    protected $fillable = ['name','email'];

    // optione 2
    // protected $guared = [];
}
