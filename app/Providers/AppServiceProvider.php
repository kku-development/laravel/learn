<?php

namespace App\Providers;

use App\Billing\BankPaymentGateway;
use App\Billing\CreditPaymentGateway;
use App\Billing\PaymentGatewayContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // PaymentGatewayContract
        // $this->app->bind(PaymentGatewayContract::class, function($app) {
        $this->app->singleton(PaymentGatewayContract::class, function($app) {
            $isCredit = json_decode(request()->credit);
            if ($isCredit) {
                return new CreditPaymentGateway('KRW');
            }
            return new BankPaymentGateway('KRW');
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
