<?php

namespace App\Orders;

use App\Billing\PaymentGatewayContract;

class OrderDetails
{
    private $PaymentGatewayContract;

    public function __construct(PaymentGatewayContract $PaymentGatewayContract)
    {
        $this->PaymentGatewayContract = $PaymentGatewayContract;
    }

    public function all()
    {
        $this->PaymentGatewayContract->setDiscount(500);

        return [
            'name' => 'kuinsoo',
            'address' => '구봉로419 101동 509호',
        ];
    }
}
