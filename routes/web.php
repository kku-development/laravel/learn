<?php

use App\Http\Controllers\CustomerController;
use Illuminate\Support\Facades\Route;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/welcome');
});

Route::get('/hello', function () {
    $body = "KKu Larvel Learn";
    $title = "KKu Island";

    // 변수명과 key 값이 같게 사용할 경우
    return view('subview/hello', compact('body', 'title'));
    // return view('subview/hello', [
    //     'title' => $title,
    //     'body'  => $body,
    // ]);
});


Route::get('/hello/hello', function () {
    return 'Hello * 2';
});

// step 1
Route::get('/step1/hello', 'HelloController@index');
Route::get('/step1/about', 'HelloController@about');
Route::get('/step1/services', 'HelloController@services');

// step 2
Route::get('/step2/service', 'ServiceController@index');
Route::post('/step2/service', 'ServiceController@store');
Route::get('/step2/about', 'AboutController@index');

//step 3
Route::get('/customers', 'CustomerController@index');
Route::get('/customers/create', 'CustomerController@create');
Route::post('/customers', 'CustomerController@store');
Route::get('/customers/{customerId}', 'CustomerController@show');
Route::get('/customers/{customerId}/edit', 'CustomerController@edit');
Route::patch('/customers/{customerId}', 'CustomerController@update');
Route::delete('/customers/{customerId}', 'CustomerController@destroy');

Route::get('/custommerTable/add/{colum}', 'CustomerController@addColum');


// step 4 Mail
Route::get('/email', function() {
    // Mail::to('kkuinsoo@gmail.com')->send(new WelcomeMail());
    Mail::to('kkuinsoo@gmail.com')->send(new WelcomeMail());
    return new WelcomeMail();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// mini project
Route::get('/questionnaires', 'QuestionnaireController@index');
Route::get('/questionnaires/create', 'QuestionnaireController@create');


// Advanced
Route::get('/pay', 'PayOrderController@store');

// View
Route::get('/channel', 'ChannelController@index');
Route::get('/post/create', 'PostController@create');
